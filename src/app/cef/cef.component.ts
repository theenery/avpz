import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';

@Component({
  selector: 'app-cue',
  templateUrl: './cef.component.html',
  styleUrls: ['./cef.component.scss']
})
export class CEFComponent implements OnInit {
  evaluations: boolean = true;
  factors: boolean = false;
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public summary = (column: number): string => {
    let vals = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];
    vals[0][0] = this.localData.reduce((p, c) => p + c.IndustryEstimation, 0) / 10;
    vals[1][0] = this.localData.reduce((p, c) => p + c.UsabilityEstimation, 0) / 10;
    vals[2][0] = this.localData.reduce((p, c) => p + c.ProgrammingEstimation, 0) / 10;
    vals[3][0] = this.localData.reduce((p, c) => p + Number.parseFloat(c.UsersEstimation), 0) / 10;
    vals[0][1] = this.localData.reduce((p, c) => p + c.IndustryWeighting, 0) / 10;
    vals[1][1] = this.localData.reduce((p, c) => p + c.UsabilityWeighting, 0) / 10;
    vals[2][1] = this.localData.reduce((p, c) => p + c.ProgrammingWeighting, 0) / 10;
    vals[3][1] = this.localData.reduce((p, c) => p + c.UsersWeighting, 0) / 10;

    for (let row of this.localData) {
      vals[4][0] += (
        row.IndustryEstimation +
        row.UsabilityEstimation +
        row.ProgrammingEstimation +
        Number.parseFloat(row.UsersEstimation)) / 4;
      vals[4][1] += (
        row.IndustryWeighting +
        row.UsabilityWeighting +
        row.ProgrammingWeighting +
        row.UsersWeighting) / 4;
    }
    vals[4][0] /= 10;
    vals[4][1] /= 10;

    if (this.evaluations) {
      if (this.factors) {
        return vals[column][1].toFixed(2) + "/" + vals[column][0].toFixed(2);
      }
      return vals[column][0].toFixed(2);
    }
    return vals[column][1].toFixed(2);
  }
  title = 'CEF';
  
  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    this.grid.summaryRowHeight = 50;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  average(row: any) {
    let estimationsAverage = ((
      row.IndustryEstimation +
      row.UsabilityEstimation +
      row.ProgrammingEstimation +
      Number.parseFloat(row.UsersEstimation)) / 4).toFixed(2);
    let weightingAverage = ((
      row.IndustryWeighting +
      row.UsabilityWeighting +
      row.ProgrammingWeighting +
      row.UsersWeighting) / 4).toFixed(2);
    if (this.evaluations) {
      if (this.factors) {
        return weightingAverage.toString() + '/' + estimationsAverage.toString();
      }
      return estimationsAverage.toString();
    }
    return weightingAverage.toString();
  }

  public cellEdit(evt: any) {
    if (!evt.valid) {
      evt.cancel = true;
    }
  }

  read(): any[] {
    let criteria = sharedData.criteria;
    let result = [];
    let counter = 1;
    for (let i = 0; i < criteria.length; i++) {
      result.push({
        Id: counter++,
        Name: criteria[i].name,
        IndustryEstimation: criteria[i].expertEstimation[0],
        UsabilityEstimation: criteria[i].expertEstimation[1],
        ProgrammingEstimation: criteria[i].expertEstimation[2],
        UsersEstimation: IgxNumberSummaryOperand.average(criteria[i].userEstimation).toFixed(2),
        IndustryWeighting: criteria[i].weightingFactor[0],
        UsabilityWeighting: criteria[i].weightingFactor[1],
        ProgrammingWeighting: criteria[i].weightingFactor[2],
        UsersWeighting: criteria[i].weightingFactor[3]
      });
    }
    return result;
  }

  setEvaluations(value: boolean) {
    this.evaluations = value;
    if (!this.evaluations && !this.factors) {
      this.factors = true;
    }
  }

  setFactors(value: boolean) {
    this.factors = value;
    if (!this.factors && !this.evaluations) {
      this.evaluations = true;
    }
  }

  write(): void {
    let criteria = sharedData.criteria;
    let counter = 0;
    for (let i = 0; i < criteria.length; i++) {
      criteria[i].expertEstimation[0] = this.localData[i].IndustryEstimation;
      criteria[i].expertEstimation[1] = this.localData[i].UsabilityEstimation;
      criteria[i].expertEstimation[2] = this.localData[i].ProgrammingEstimation;
      
      criteria[i].weightingFactor[0] = this.localData[i].IndustryWeighting;
      criteria[i].weightingFactor[1] = this.localData[i].UsabilityWeighting;
      criteria[i].weightingFactor[2] = this.localData[i].ProgrammingWeighting;
      criteria[i].weightingFactor[3] = this.localData[i].UsersWeighting;
      counter++;0
    }
  }
}
