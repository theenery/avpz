import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';

@Component({
  selector: 'app-rv',
  templateUrl: './rv.component.html',
  styleUrls: ['./rv.component.scss']
})
export class RVComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public summary =  class {
      public operate(data?: any[], allGridData: any[] = []): IgxSummaryResult[] {
        const result = [];

        let VRERs: number[] = [];
        allGridData.forEach(e => VRERs.push(e.ER * e.LRER));
        
        result.push({
          key: 'min',
          label: 'Min:',
          summaryResult: (IgxNumberSummaryOperand.min(VRERs)).toFixed(2)
        });

        result.push({
          key: 'maz',
          label: 'Max:',
          summaryResult: (IgxNumberSummaryOperand.max(VRERs).toFixed(2))
        });
        return result;
      }
    };
  title = 'RV';
  
  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  average(array: any) {
    return IgxNumberSummaryOperand.average(array);
  }

  public cellEdit(evt: any) {
    if (!evt.valid) {
      evt.cancel = true;
    }
  }

  getPriority(row: any) {
    let VRER = row.ER * row.LRER;
    let VRERs: number[] = [];
    this.localData.forEach(e => VRERs.push(e.ER * e.LRER));
    let min = IgxNumberSummaryOperand.min(VRERs);
    let max = IgxNumberSummaryOperand.max(VRERs);
    let step = (max - min) / 3;
    if (VRER < min + step) {
      return 'Низький';
    }
    else if (VRER > max - step) {
      return 'Високий';
    }
    else {
      return 'Середній';
    }
  }

  read(): any[] {
    let risks = sharedData.risks;
    let result = [];
    let counter = 1;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        result.push({
          Id: counter++,
          Event: risks[i].Event,
          Tag: risks[i].Tag,
          TagId: j + 1,
          Description: risks[i].Events[j].Description,
          LRER: risks[i].Events[j].LRER,
          ER: this.average(risks[i].Events[j].Probabilities)
        });
      }
    }
    return result;
  }

  write(): void {
    let risks = sharedData.risks;
    let counter = 0;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        risks[i].Events[j].LRER = this.localData[counter].LRER;
        counter++;
      }
    }
  }
}
