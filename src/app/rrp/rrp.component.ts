import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';

@Component({
  selector: 'app-rrp',
  templateUrl: './rrp.component.html',
  styleUrls: ['./rrp.component.scss']
})
export class RRPComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public summary =  class {
      public operate(data?: any[], allGridData = []): IgxSummaryResult[] {
        const result = [];

        let sum = 0;
        for (let i = 0; i < allGridData.length; i++) {
          let values: any[] = [];
          for (let k = 0; k < 10; k++) {
            values.push(allGridData[i]['Value' + k]);
          }
          sum += IgxNumberSummaryOperand.sum(values) / 10;
        }
        sum /= allGridData.length;
        
        result.push({
          key: 'probability',
          label: 'Ймовірність:',
          summaryResult: (sum * 100).toFixed(2) + '%'
        });
        return result;
      }
    };
  title = 'RRP';
  
  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  average(row: any) {
    let values = [];
    for (let k = 0; k < 10; k++) {
      values.push(row['Value' + k]);
    }
    return IgxNumberSummaryOperand.average(values).toFixed(2);
  }

  public cellEdit(evt: any) {
    if (!evt.valid) {
      evt.cancel = true;
    }
  }

  read(): any[] {
    let risks = sharedData.risks;
    let result = [];
    let counter = 1;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        let obj = {
          Id: counter++,
          Event: risks[i].Event,
          Tag: risks[i].Tag,
          TagId: j + 1,
          Description: risks[i].Events[j].Description
        };
        for (let k = 0; k < 10; k++) {
          Object.defineProperty(obj, 'Value' + k, { writable: true, value: risks[i].Events[j].ProbabilitiesAfter[k] });
        }
        result.push(obj);
      }
    }
    return result;
  }

  write(): void {
    let risks = sharedData.risks;
    let counter = 0;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        let values = [];
        for (let k = 0; k < 10; k++) {
          values.push(this.localData[counter]['Value' + k]);
        }
        risks[i].Events[j].ProbabilitiesAfter = values;
        counter++;
      }
    }
  }
}
