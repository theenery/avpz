import { Component, OnInit, ViewChild } from '@angular/core';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';
import { sharedData } from '../sharedData';

@Component({
  selector: 'app-ctf',
  templateUrl: './ctf.component.html',
  styleUrls: ['./ctf.component.scss']
})
export class CTFComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  title = 'CTF';

  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    this.grid.summaryRowHeight = 50;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  getAvgRel() {
    console.log(this.localData, this.localData.reduce((p, c) => p + c.value, 0) / 10)
    return this.localData.reduce((p, c) => p + Number.parseFloat(c.value), 0) / 10;
  }

  read(): any[] {
    let result = [];
    for (let i = 0; i < sharedData.expertsWeighting.length; i++) {
      result.push({
        Id: i,
        name: sharedData.expertsWeighting[i].name,
        value: sharedData.expertsWeighting[i].value
      });
    }
    return result;
  }

  write(): void {
    for (let i = 0; i < sharedData.expertsWeighting.length; i++) {
      sharedData.expertsWeighting[i].value = this.localData[i].value;
    }
  }
}
