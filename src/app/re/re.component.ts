import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';

@Component({
  selector: 'app-re',
  templateUrl: './re.component.html',
  styleUrls: ['./re.component.scss']
})
export class REComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public summary = class {
    public operate(data?: any[]): IgxSummaryResult[] {
      const result = [];
      data = data?.map((e) => e ? 1 : 0);
      result.push({
        key: 'probability',
        label: 'Ймовірність:',
        summaryResult: (IgxNumberSummaryOperand.sum(data ?? []) / sharedData.risks.flatMap(r => r.Events).length * 100).toFixed(2) + '%'
      });
      return result;
    }
  };
  title = 'RE';

  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  read(): any[] {
    let risks = sharedData.risks;
    let result = [];
    let counter = 1;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        result.push({
          Id: counter++,
          Event: risks[i].Event,
          Tag: risks[i].Tag,
          TagId: j + 1,
          Description: risks[i].Events[j].Description,
          Value: risks[i].Events[j].Value
        });
      }
    }
    return result;
  }

  write(): void {
    let risks = sharedData.risks;
    let counter = 0;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        risks[i].Events[j].Value = this.localData[counter++].Value;
      }
    }
  }
}
