import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, NgFor } from "@angular/common";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RSComponent } from './rs/rs.component';
import { IgxGridModule, IgxCheckboxModule } from 'igniteui-angular';
import { FormsModule } from '@angular/forms';
import { ChartComponent } from './chart/chart.component';
import { IgxCategoryChartModule, IgxFinancialChartModule } from 'igniteui-angular-charts';
import { IgxLegendModule, IgxDataChartCoreModule, IgxDataChartPolarModule, IgxDataChartPolarCoreModule, IgxDataChartInteractivityModule, IgxDataChartAnnotationModule } from 'igniteui-angular-charts';
import { REComponent } from './re/re.component';
import { OutletComponent } from './outlet/outlet.component';
import { RPComponent } from './rp/rp.component';
import { RVComponent } from './rv/rv.component';
import { RLComponent } from './rl/rl.component';
import {
  IgxButtonModule,
  IgxIconModule,
  IgxLayoutModule,
  IgxNavigationDrawerModule,
  IgxRippleModule,
  IgxToggleModule,
  IgxTreeModule,
  IgxSelectModule
} from "igniteui-angular";
import { RRPComponent } from './rrp/rrp.component';
import { RRVComponent } from './rrv/rrv.component';
import { CUEComponent } from './cue/cue.component';
import { CEFComponent } from './cef/cef.component';
import { CTFComponent } from './ctf/ctf.component';
import { CCVComponent } from './ccv/ccv.component';

@NgModule({
  declarations: [
    AppComponent,
    RSComponent,
    REComponent,
    ChartComponent,
    OutletComponent,
    RPComponent,
    RVComponent,
    RLComponent,
    RRPComponent,
    RRVComponent,

    CUEComponent,
    CEFComponent,
    CTFComponent,
    CCVComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IgxGridModule,
    IgxCheckboxModule,
    FormsModule,
    IgxCategoryChartModule,
    IgxFinancialChartModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    IgxLegendModule,
    IgxDataChartCoreModule,
    IgxDataChartPolarModule,
    IgxDataChartPolarCoreModule,
    IgxDataChartInteractivityModule,
    IgxDataChartAnnotationModule,
	IgxButtonModule,
    IgxIconModule,
    IgxLayoutModule,
    IgxNavigationDrawerModule,
    IgxRippleModule,
    IgxToggleModule,
    IgxTreeModule,
    IgxSelectModule,
    NgFor
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
