import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RSComponent } from './rs/rs.component';
import { ChartComponent } from './chart/chart.component';
import { REComponent } from './re/re.component';
import { OutletComponent } from './outlet/outlet.component';
import { RPComponent } from './rp/rp.component';
import { RVComponent } from './rv/rv.component';
import { RLComponent } from './rl/rl.component';
import { RRPComponent } from './rrp/rrp.component';
import { RRVComponent } from './rrv/rrv.component';
import { CUEComponent } from './cue/cue.component';
import { CEFComponent } from './cef/cef.component';
import { CTFComponent } from './ctf/ctf.component';
import { CCVComponent } from './ccv/ccv.component';

export const routes: Routes = [
  {
    path: 'identification', component: OutletComponent, data: { displayName: 'Ідентифікація' }, children: [
      { path: 'risk-sources', component: RSComponent, data: { displayName: 'Джерела ризиків' } },
      { path: 'risk-events', component: REComponent, data: { displayName: 'Ризикові події' } }
    ]
  },
  {
    path: 'analysis', component: OutletComponent, data: { displayName: 'Аналіз' }, children: [
      { path: 'risk-probability', component: RPComponent, data: { displayName: 'Ймовірності ризиків' } },
      { path: 'risk-values', component: RVComponent, data: { displayName: 'Величини та пріоритети' } }
    ]
  },
  {
    path: 'planning', component: OutletComponent, data: { displayName: 'Планування' }, children: [
      { path: 'risk-liquidation', component: RLComponent, data: { displayName: 'Усунення ризиків' } }
    ]
  },
  {
    path: 'monitoring', component: OutletComponent, data: { displayName: 'Моніторинг' }, children: [
      { path: 'risk-probability-after', component: RRPComponent, data: { displayName: 'Ймовірності ризиків' } },
      { path: 'risk-values-after', component: RRVComponent, data: { displayName: 'Величини та пріоритети' } }
    ]
  },

  {
    path: 'evaluations', component: OutletComponent, data: { displayName: 'Оцінювання' }, children: [
      { path: 'users', component: CUEComponent, data: { displayName: 'Оцінки користувачів' } },
      { path: 'experts-and-factors', component: CEFComponent, data: { displayName: 'Оцінки експертів та вагові коефіцієнти' } },
      { path: 'types-and-factors', component: CTFComponent, data: { displayName: 'Типи експертів та їх вагомості' } },
      { path: 'complex-values', component: CCVComponent, data: { displayName: 'Комплексні показники' } },
      { path: 'chart', component: ChartComponent, data: { displayName: 'Полярні діаграми' } }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
