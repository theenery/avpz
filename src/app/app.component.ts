import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { routes } from './app-routing.module';
import { IgxTreeComponent } from 'igniteui-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'AVPZProjekt';
  public routes = routes;
  @ViewChild('tree') public tree!: IgxTreeComponent;
  public selectedContent = '';
  
  public ngAfterViewInit() {
    this.tree.activeNodeChanged.subscribe(node => {
      if (node.allChildren?.length == 0) {
        this.selectedContent = node.data;
      }
    });
  }
}
