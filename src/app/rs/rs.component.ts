import { Component, OnInit, ViewChild } from '@angular/core';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';
import { sharedData } from '../sharedData';

@Component({
  selector: 'app-rs',
  templateUrl: './rs.component.html',
  styleUrls: ['./rs.component.scss']
})
export class RSComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public summary = class {
    public operate(data?: any[]): IgxSummaryResult[] {
      const result = [];
      data = data?.map((e) => e ? 1 : 0);
      result.push({
        key: 'probability',
        label: 'Ймовірність:',
        summaryResult: (IgxNumberSummaryOperand.sum(data ?? []) / sharedData.risks.flatMap(r => r.Sources).length * 100).toFixed(2) + '%'
      });
      return result;
    }
  };
  title = 'RS';

  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  read(): any[] {
    let risks = sharedData.risks;
    let result = [];
    let counter = 1;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Sources.length; j++) {
        result.push({
          Id: counter++,
          Source: risks[i].Source,
          Tag: risks[i].Tag,
          TagId: j + 1,
          Description: risks[i].Sources[j].Description,
          Value: risks[i].Sources[j].Value
        });
      }
    }
    return result;
  }

  write(): void {
    let risks = sharedData.risks;
    let counter = 0;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Sources.length; j++) {
        risks[i].Sources[j].Value = this.localData[counter++].Value;
      }
    }
  }
}
