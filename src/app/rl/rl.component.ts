import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, IgxSummaryResult } from 'igniteui-angular';

@Component({
  selector: 'app-rl',
  templateUrl: './rl.component.html',
  styleUrls: ['./rl.component.scss']
})
export class RLComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  public solutions = sharedData.solutions;
  title = 'RP';

  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }


  read(): any[] {
    let risks = sharedData.risks;
    let result = [];
    let counter = 1;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        result.push({
          Id: counter++,
          Event: risks[i].Event,
          Tag: risks[i].Tag,
          TagId: j + 1,
          Description: risks[i].Events[j].Description,
          Solution: this.solutions[risks[i].Events[j].Solution]
        });
      }
    }
    return result;
  }

  write(): void {
    let risks = sharedData.risks;
    let counter = 0;
    for (let i = 0; i < risks.length; i++) {
      for (let j = 0; j < risks[i].Events.length; j++) {
        risks[i].Events[j].Solution = this.solutions.indexOf(this.localData[counter].Solution);
        counter++;
      }
    }
    console.log(risks)
  }
}
