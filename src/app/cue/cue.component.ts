import { Component, OnInit, ViewChild } from '@angular/core';
import { sharedData } from '../sharedData';
import { IgxGridComponent, IgxNumberSummaryOperand, /*IgxSummaryResult*/ } from 'igniteui-angular';

@Component({
  selector: 'app-cue',
  templateUrl: './cue.component.html',
  styleUrls: ['./cue.component.scss']
})
export class CUEComponent implements OnInit {
  @ViewChild('grid', { read: IgxGridComponent, static: true })
  public grid!: IgxGridComponent;
  public localData: any[] = [];
  //public summary =  class {
  //    public operate(data?: any[], allGridData = []): IgxSummaryResult[] {
  //      const result = [];

  //      let sum = 0;
  //      for (let i = 0; i < allGridData.length; i++) {
  //        let values: any[] = [];
  //        for (let k = 0; k < 10; k++) {
  //          values.push(allGridData[i]['Value' + k]);
  //        }
  //        sum += IgxNumberSummaryOperand.sum(values) / 10;
  //      }
  //      sum /= allGridData.length;
        
  //      result.push({
  //        key: 'probability',
  //        label: 'Ймовірність:',
  //        summaryResult: (sum * 100).toFixed(2) + '%'
  //      });
  //      return result;
  //    }
  //  };
  title = 'CUE';
  
  ngOnInit(): void {
    this.localData = this.read();
    this.grid.showSummaryOnCollapse = true;
    
    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  average(row: any) {
    let values = [];
    for (let k = 0; k < 20; k++) {
      values.push(row['Value' + k]);
    }
    return IgxNumberSummaryOperand.average(values).toFixed(2);
  }

  public cellEdit(evt: any) {
    if (!evt.valid) {
      evt.cancel = true;
    }
  }

  read(): any[] {
    let criteria = sharedData.criteria;
    let result = [];
    let counter = 1;
    for (let i = 0; i < criteria.length; i++) {
      let obj = {
        Id: counter++,
        Name: criteria[i].name
      };
      for (let k = 0; k < 20; k++) {
        Object.defineProperty(obj, 'Value' + k, { writable: true, value: criteria[i].userEstimation[k] });
      }
      result.push(obj);
    }
    return result;
  }

  write(): void {
    let criteria = sharedData.criteria;
    let counter = 0;
    for (let i = 0; i < criteria.length; i++) {
      let values = [];
      for (let k = 0; k < 20; k++) {
        values.push(this.localData[counter]['Value' + k]);
      }
      criteria[i].userEstimation = values;
      counter++;
    }
  }
}
