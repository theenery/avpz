import { OnInit, Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { localData } from './localData';
import { IgxLegendComponent, IgxDataChartComponent, IgxNumericAngleAxisComponent, IgxNumericRadiusAxisComponent, IgxPolarAreaSeriesComponent, IgxDataToolTipLayerComponent } from 'igniteui-angular-charts';
import { sharedData } from '../sharedData';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent {

  public constructor(private _detector: ChangeDetectorRef) {

  }

  @ViewChild("legend", { static: true })
  private legend!: IgxLegendComponent
  @ViewChild("chart", { static: true })
  private chart!: IgxDataChartComponent
  @ViewChild("angleAxis", { static: true })
  private angleAxis!: IgxNumericAngleAxisComponent
  @ViewChild("radiusAxis", { static: true })
  private radiusAxis!: IgxNumericRadiusAxisComponent
  @ViewChild("polarAreaSeries1", { static: true })
  private polarAreaSeries1!: IgxPolarAreaSeriesComponent
  @ViewChild("polarAreaSeries2", { static: true })
  private polarAreaSeries2!: IgxPolarAreaSeriesComponent
  @ViewChild("dataToolTipLayer", { static: true })
  private dataToolTipLayer!: IgxDataToolTipLayerComponent

  externalData!: { criteria: any[], expertsWeighting: any[] };
  gridData: any[] = [];
  localData = localData;
  show: boolean[] = [true, true, false, false, false];

  ngOnInit() {
    this.externalData = this.read();
    for (let i = 0; i < 5; i++) { this.calculateFor(i); } 

    let watermarks = document.getElementsByTagName('igc-trial-watermark');
    Array.from(watermarks).forEach(e => e.setAttribute('hidden', 'true'));
  }

  calculateFor(idx: number) {
    let factors = this.externalData.criteria.map(e => e.weightingFactor[idx]);
    let sumFactors = factors.reduce((p, c) => p + c, 0);
    let parts = factors.map(e => e / sumFactors * 360);
    let start = -parts[0] / 2;
    let bounds: number[] = [];
    parts.forEach(p => bounds.push(p + (bounds.length ? bounds[bounds.length - 1] : start)));
    let degrees: number[] = [];
    bounds.forEach((b, i) => degrees.push((b + (i ? bounds[i - 1] : start)) / 2));
    let rads = degrees.map(d => Math.PI * d / 180);

    let xwqs = idx != 4 ?
      (factors.map((f, i) => f * this.externalData.expertsWeighting[idx].value * this.externalData.criteria[i].expertEstimation[idx] / 10)) :
      (factors.map((f, i) => [0, 1, 2, 3].reduce((p, j) => p + this.externalData.expertsWeighting[j].value * this.externalData.criteria[i].expertEstimation[j] * this.externalData.criteria[i].weightingFactor[j], 0) / this.externalData.expertsWeighting.reduce((p, c) => p + c.value, -this.externalData.expertsWeighting[4].value)));
    let as = xwqs.map((v, i) => v * Math.sin(rads[i]));
    let bs = xwqs.map((v, i) => v * Math.cos(rads[i]));
    let cs = as.map((v, i) => Math.sqrt(v * v + bs[i] * bs[i])); // same as xwqs

    let sqs = cs.map((v, i) => {
        let next = i == cs.length - 1 ? 0 : i + 1;
        return Math.abs(as[i] * bs[next] - as[next] * bs[i]);
    });
    let S = sqs.reduce((p, c) => p + c, 0) / 2;
    let z = S / (Math.PI * 100 * 100);

    this.gridData.push({
      idx,
      S,
      z,
      name: this.externalData.expertsWeighting[idx].name,
      value: this.externalData.expertsWeighting[idx].value
    })

    for (let k = 0; k < 10; k++) {
      let obj = this.localData[k];
      Object.defineProperty(obj, 'angle' + idx, { writable: true, value: degrees[k] });
      Object.defineProperty(obj, 'radius' + idx, { writable: true, value: xwqs[k] });
    }
  }

  read() {
    let expertsWeighting = [];
    for (let i = 0; i < sharedData.expertsWeighting.length; i++) {
      expertsWeighting.push({
        id: i,
        name: sharedData.expertsWeighting[i].name,
        value: sharedData.expertsWeighting[i].value
      });
    }
    expertsWeighting.push({
      id: 4,
      name: 'Усереднені оцінки',
      value: expertsWeighting.reduce((p, c) => p + c.value, 0) / 4
    })
    let criteria = [];
    for (let i = 0; i < sharedData.criteria.length; i++) {
      let expertEstimation = [...sharedData.criteria[i].expertEstimation];
      expertEstimation.push(sharedData.criteria[i].userEstimation.reduce((p, v) => p + v, 0) / 20);
      expertEstimation.push(expertEstimation.reduce((p, v) => p + v, 0) / 4);
      let weightingFactor = [...sharedData.criteria[i].weightingFactor];
      weightingFactor.push(weightingFactor.reduce((p, v) => p + v, 0) / 4);
      criteria.push({
        id: i,
        expertEstimation: expertEstimation,
        name: sharedData.criteria[i].name,
        weightingFactor: weightingFactor
      });
    }
    return { criteria, expertsWeighting };
  }

  spec(idx: number): number {
    if (idx == 4) {
      return (this.spec(0) + this.spec(1) + this.spec(2) + this.spec(3)) / 4;
    }
    return this.gridData[idx].S * this.gridData[idx].value / 10;
  }
}
